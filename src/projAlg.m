% Copyright (c) 2014-2015 Michigan State University and the CHARMS Research
% Group
% 
% This file is part of the BlockPR software package
% 
% Permission is hereby granted, free of charge, to any person obtaining a copy of
% this software and associated documentation files (the "Software"), to deal in
% the Software without restriction, including without limitation the rights to
% use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
% of the Software, and to permit persons to whom the Software is furnished to do
% so, subject to the following conditions:
% 
% The above copyright notice and this permission notice shall be included in all
% copies or substantial portions of the Software.
% 
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
% IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
% OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
% SOFTWARE.

function projSoln = projAlg( initGuess, measurements, maskEntries, pars )


%% Initializations

% Problem dimension
d = pars.d;

% No. of masks used
nMasks = size(maskEntries, 1);

% We will work with absolute value (and not squared absolute value) of the
% measurements
measurements = sqrt(measurements);

% Store result of applying forward operator/measurement matrix here
estMeas = zeros(d, nMasks);

% Store fft of the masks
maskFFT = zeros(d, nMasks);
for idMasks = 1:nMasks
    maskFFT(:,idMasks) = fft( conj(maskEntries(idMasks, :).'), d );
end

% Normalization factor for aplpying the (pseudo) inverse transform
normFac = sum( abs(maskFFT).^2, 2 );

% Max. iterations
nIter = pars.altProjIters;

% Start with initial guess
projSoln = full(initGuess);

% In each iteration, ...
for idx = 1:nIter

    %% Forward operator
    % For efficiency, compute these using FFTs
    % Recall that our measurements are correlations, which can be
    % efficiently evaluated using FFTs
    % Pre-store FFT of the current guess
    fftFun = ifft(projSoln);

    for idMasks = 1:nMasks
        % Measurements 
        % Structure of measurements - |corr(y,w)| = b^i, i = 1,...,P
        % We express the correlation as a matrix multiplication
        % This matrix has a circulant structure (generated using gallery)
        % measurements(:,idx) = abs( gallery('circul', mask(:,idx)) * ...
        %                                                currentGuess );
        % The multiplication with a circulant matrix can be efficiently
        % implemented using FFTs
        % (discrete circular correlation/convolutions)         
        estMeas(:,idMasks) = fft( fftFun .* maskFFT(:,idMasks) );
    end

    
    %% First projection - Set magnitudes using measurements
    phase = angle(estMeas);
    estMeas = measurements .* exp(1i*phase);
    
    
    %% Second projection - Apply adjoint operator
    % We may compute these effectively using FFTs
        
    prevSoln = projSoln;                % Store previous iterate solution
    
    % Pseudo-inverse operator
    projSoln = zeros(d,1);
    for idMasks = 1:nMasks
        projSoln = projSoln + ...
            fft( (conj(maskFFT(:,idMasks))./normFac) .* ...
                                        ifft( estMeas(:,idMasks) ) );
        
    end
    
    if( norm(prevSoln-projSoln) <= pars.threshold )
        break;
    end
end

end

