% Copyright (c) 2016- Michigan State University and the CHARMS Research
% Group
% 
% This file is part of the BlockPR software package
% 
% Permission is hereby granted, free of charge, to any person obtaining a copy of
% this software and associated documentation files (the "Software"), to deal in
% the Software without restriction, including without limitation the rights to
% use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
% of the Software, and to permit persons to whom the Software is furnished to do
% so, subject to the following conditions:
% 
% The above copyright notice and this permission notice shall be included in all
% copies or substantial portions of the Software.
% 
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
% IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
% OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
% SOFTWARE.

%% EigenAngularSync.m
%
% This function solves the angular synchronization problem,
%
% Estimate d unknown angles theta_1, theta_2, ... , \theta_d \in [0, 2π) 
% from d(2\delta - 1) (noisy) measurements of their differences
%   \Delta \theta_{ij} := \theta_i - \theta_j
%
% by using an eigenvector method (we compute the leading eigenvector). 
% This function expects the input (scaled) phase differences to be 
% arranged in a banded matrix of the form
%
%                   {   (xx*)j,k        if |j- k mod d| < \delta 
%       (X)j,k =    {
%                   {       0           else
%
% where x \in C^d and X \in C^{d x d}.
%
% Usage:
%   RecoveredSoln = EigenAngularSync( BandedMat, pars)
%
% Inputs:
%   bandedMat   -   Banded matrix of scaled Phase differences. See above
%                   for structure. Each row has 2\delta-1 non-zero entries 
%                   of the form (X)j,k = x_jx_k^*.
%                   Matrix, double, complex, dxd
%
%   pars        -   Matlab structure containing problem parameters.
%                   See <root>/src/PrintProblemPars.m for elements.
%
% Output(s):
%   recoveredSoln   -   recovered solution 
%                       Vector, double, complex, dx1
%

function RecoveredSoln = EigenAngularSync( BandedMat, pars )

%% Initialization

d       = pars.d;                 % Problem dimension
delta   = pars.delta;             % Block/band parameter

% Estimate magnitude of unknown vector
EstMag  = EstimateMagnitude( BandedMat, pars );


%% Angular Synchronization

% Eigenvector-based angular synchronization

switch lower(pars.EigSolver)
    
    case 'eigs'
        % Use Matlab's eigs solver
        
        % Options to Matlab's eigen solver
        opts.isreal = false;      % We have complex entries in our matrix
        opts.issym = true;        % Hermitian symmetric
    %TODO: Is this the best number of iterations?
    %     opts.maxit = 5000;
    %     opts.maxit = max(20, floor(10.0*d/(2*delta)) );   % Max. iterations
    %     if( pars.addNoise )             % In noisy simulations, only run eigen 
    %         % TOFIX                     % solver to roughly the level of noise
    %         opts.tol = 1e-3*sqrt( 1/( 10^(pars.snr/10) ) );
    %     end    

    % Initial guess
    % Rather than start with a random guess, lets start with one of the
    % standard basis elements, e_k, k = max_i |x_i|
    %     e_k = zeros(d, 1);
    %     [~, maxloc] = max(magnitudes);
    %     e_k(maxloc) = 1;
    %     opts.v0 = e_k;
    %     opts.v0 = zeros(d, 1) + 1i*zeros(d, 1);
    %     opts.v0 = opts.v0 ./ abs(opts.v0);

    % Power method - compute top e-vec
    %     % Custom implementation of the power method
    %     recoveredSoln = opts.v0;
    %     for ia = 1:100;%opts.maxit
    %         recoveredSoln = phaseDiffs_banded*recoveredSoln;
    %         recoveredSoln = recoveredSoln/norm(recoveredSoln);
    %     end
    % Use Matlab's implementation
    [RecoveredSoln, ~, flag] = eigs(phaseDiffs_banded, 1, 'LM', opts);    
    if(flag ~= 0 )
        fprintf( '\n **Error!** eigs did not converge! \n' );
    end

    %     [V, D] = eig( full(phaseDiffs_banded) );
    %     [~, maxloc] = max(diag(D));
    %     recoveredSoln = V(:,maxloc);

% Retain the phase of the eigen vector solution
nz_idx = find(RecoveredSoln);       % of only the non-zero entries
RecoveredSoln(nz_idx) = RecoveredSoln(nz_idx) ./ ...
                                    abs(RecoveredSoln(nz_idx));
RecoveredSoln = RecoveredSoln .* magnitudes;


return