% Copyright (c) 2015 Michigan State University and the CHARMS Research
% Group
% 
% This file is part of the BlockPR software package
% 
% Permission is hereby granted, free of charge, to any person obtaining a copy of
% this software and associated documentation files (the "Software"), to deal in
% the Software without restriction, including without limitation the rights to
% use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
% of the Software, and to permit persons to whom the Software is furnished to do
% so, subject to the following conditions:
% 
% The above copyright notice and this permission notice shall be included in all
% copies or substantial portions of the Software.
% 
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
% IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
% OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
% SOFTWARE.

%% generateTestFunction.m
%
% This function generates a (1D) test function for studying the phase 
% retrieval problem
%
% Usage:
%   testFnc = generateTestFunction(pars)
%
% Inputs:
%   pars    -   Matlab structure containing problem parameters.
%               Structure elements are
%                   'd'             - Problem dimension (integer)
%                   'delta'         - Block length/parameter (integer)
%                   'addNoise'      - Noisy simulation? (boolean)
%                   'snr'           - If noisy simulation, added noise SNR 
%                                     (in dB) (double)
%                   'testFncType'   - type of test function (string)
%                   'nComps'        - for particular test functions 
%                                     (integer)
%                   'useFlattening' - use fast JL flattening? (boolean)
%                   'maskType'      - type of masks (string)
%                   'over'          - oversampling factor (double)
%                   'angSyncMethod' - angular synchronization method
%                                     (string)
%                   'threshold'     - noise threshold for angular
%                                     synchronization algorithms (double)
%                   'altProj'       - use alternating projection
%                                     post-processing? (boolean)
%
% Output(s):
%   testFnc     - generated test function
%

function testFnc = generateTestFunction(pars)

%% Initialization
d = pars.d;                     % Problem dimension
fncType = pars.testFncType;     % Test function type

%% Generate test signal
switch lower(fncType)
    case 'randgauss'
        % Random (complex) Gaussian test function
        testFnc = randn(d,1) + 1i*randn(d,1);
    case 'unirand'
        % Uniform (complex) Random test function in [-0.5,0.5]
        testFnc = (rand(d,1) - 0.5) + 1i*(rand(d,1) - 0.5);
    case 'sparse'
        % Sparse (complex) test function
        % There are 'nComp' non-zero entries
        nComps = pars.nComps;
        % Each is a (complex) uniform random entry in [-0.5,0.5]
        testFnc = zeros(d,1);
        testFnc( randperm(d,nComps) ) = (rand(nComps,1) - 0.5) + ...
                1i*(rand(nComps,1) - 0.5);

    case 'sinusoid'
        % Trigonometric test function
        % There are 'nComp' frequencies
        nComps = pars.nComps;
        % Amplitude of each component is uniform random (and independent)
        % in [-0.5,0.5]
        % Frequencies randomly selected in [1,floor(d/2)]
        testFnc = zeros(d,1);
        nodes = (0:d-1).';
        for idx =1:nComps
            testFnc = testFnc + ...
                        (rand-0.5)*exp(2*pi*randi(floor(d/2))*nodes/d);
        end
           
end


end

