function [recoveredSoln, exectime] = GerchSax( measurements, ...
                                                maskEntries, pars )

                
%% Phase Retrieval using the (Gerchberg-Saxton) alternating projections
%

% Initialization
switch pars.initGuess
    
    case 'zeros'
        recoveredSoln = zeros(pars.d, 1);
    case 'randn'
        recoveredSoln = sqrt(1/2)*( randn(pars.d,1) + 1i*randn(pars.d, 1) );
    case 'std_basis'
        recoveredSoln = zeros(pars.d, 1);
        recoveredSoln(1) = 1;
        
end

tic
recoveredSoln = altProj_BlockPR( ...
                recoveredSoln, ...      % Initial guess
                measurements, ...       % Measurements
                maskEntries, ...        % Measurement masks
                pars ...                % Problem parameters
                );
exectime = toc;


return