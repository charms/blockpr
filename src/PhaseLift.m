function [recoveredSoln, exectime] = PhaseLift( measurements, ...
                                            measurementMat, pars )

%% Phase Retrieval using PhaseLift
%

tic; 

switch pars.solver
    
    case 'CVX'
    % We implement PhaseLift as a trace regularized least-squares problem in
    % CVX

    cvx_begin sdp
        % If you do not have access to the MOSEK solver (MOSEK ApS or CVX 
        % Professional license), comment this line
%         cvx_solver mosek
        cvx_quiet true;
        variable X(pars.d,pars.d) hermitian

        minimize 0.5*norm( diag(measurementMat * X * measurementMat') - ...
                            measurements ) + pars.lambda*trace( X )
        subject to
            X >= 0;
    cvx_end

    % The above SDP recovers the matrix X = xx*; we need to extract x
    % Since X is low-rank (ideally rank-1), choose solution to be (scaled) 
    % leading eigenvector                                        
    [recoveredSoln, eVal] = eig(X);
    recoveredSoln = sqrt(eVal(end))*recoveredSoln(:,end);

    case 'TFOCS'
    % We'll implement this as a trace regularized least-squares problem in
    % TFOCS
    
    % Set TFOCS parameters
    % TODO: Move this to main parameters structure
    opts.maxIts     = 1e3;
    opts.tol        = 1e-10;
    % opts.restart    = 200;
    % opts.largescale = true;
    opts.printEvery = 0;        % Suppress TFOCS status messages/output
    
    % Initial guess
    initGuess = zeros(pars.d);
    
    % TFOCS requires special initialization of the measurement model
    D = size(measurements, 1);
    tfocsLinOp = initializeLinopPR( {[pars.d, pars.d], D}, ...
                                                    measurementMat ); 
    X = solver_TraceLS( tfocsLinOp, measurements, ...
                                            pars.lambda, initGuess, opts );
    % The above SDP recovers the matrix X = xx*; we need to extract x
    % Since X is low-rank (ideally rank-1), choose solution to be (scaled) 
    % leading eigenvector                                        
    [recoveredSoln, eVal] = eig(X);
%     % Use the power method
%     % Initial guess
%     % Rather than start with a random guess, lets start with one of the
%     % standard basis elements, e_k, k = max_i |x_i|
%     e_k = zeros(d, 1);
%     [~, maxloc] = max(diag(sqrt(abs(X))));
%     e_k(maxloc) = 1;
%     opts.v0 = e_k;
%     opts.isreal = false;            % We have complex entries in our matrix
%     opts.issym = true;              % Hermitian symmetric    
%     % Power method - compute top e-vec
%     [recoveredSoln, eval] = eigs(X, 1, 'LM', opts);
%     recoveredSoln = sqrt(eVal)*recoveredSoln;
    recoveredSoln = sqrt(eVal(end))*recoveredSoln(:,end);
        
end

exectime = toc;

return