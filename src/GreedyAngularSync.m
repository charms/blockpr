% Copyright (c) 2016- Michigan State University and the CHARMS Research
% Group
% 
% This file is part of the BlockPR software package
% 
% Permission is hereby granted, free of charge, to any person obtaining a copy of
% this software and associated documentation files (the "Software"), to deal in
% the Software without restriction, including without limitation the rights to
% use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
% of the Software, and to permit persons to whom the Software is furnished to do
% so, subject to the following conditions:
% 
% The above copyright notice and this permission notice shall be included in all
% copies or substantial portions of the Software.
% 
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
% IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
% OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
% SOFTWARE.

%% GreedyAngularSync.m
%
% This function solves the angular synchronization problem,
%
% Estimate d unknown angles theta_1, theta_2, ... , \theta_d \in [0, 2π) 
% from d(2\delta - 1) (noisy) measurements of their differences
%   \Delta \theta_{ij} := \theta_i - \theta_j
%
% using a greedy procedure. This function expects the input (scaled) phase
% differences to be arranged in a banded matrix of the form
%
%
%                   {   (xx*)j,k        if |j- k mod d| < \delta 
%       (X)j,k =    {
%                   {       0           else
%
% where x \in C^d and X \in C^{d x d}.
%
% Usage:
%   RecoveredSoln = GreedyAngularSync( BandedMat, pars)
%
% Inputs:
%   bandedMat   -   Banded matrix of scaled Phase differences. See above
%                   for structure. Each row has 2\delta-1 non-zero entries 
%                   of the form (X)j,k = x_jx_k^*.
%                   Matrix, double, complex, dxd
%
%   pars        -   Matlab structure containing problem parameters.
%                   See <root>/src/PrintProblemPars.m for elements.
%
% Output(s):
%   recoveredSoln   -   recovered solution 
%                       Vector, double, complex, dx1
%

function RecoveredSoln = GreedyAngularSync( BandedMat, pars )

%% Initialization

d       = pars.d;                 % Problem dimension
delta   = pars.delta;             % Block/band parameter

% Estimate magnitude of unknown vector
EstMag  = EstimateMagnitude( BandedMat, pars );


%% Angular Synchronization

% Normalize entries to unit magnitude
nz_idx = find(BandedMat);

% Do we use thresholding?
if (pars.thresholding)
    % for thresholding purposes, record magnitudes ...
    idx_threshold = find( abs(BandedMat(nz_idx)) <= pars.threshold );
    
    % now threshold 
    % ... this normalizes only entries on the band
    BandedMat(nz_idx) = BandedMat(nz_idx) ./ abs( BandedMat(nz_idx) );
    % ... this sets the small entries to zero
    BandedMat( idx_threshold ) = 0;
else
    % Normalize all entries in the 2delta-1 band
    BandedMat(nz_idx) = BandedMat(nz_idx) ./ abs( BandedMat(nz_idx) );
end
    

% Greedy Angular Synchronization
% Initialize solution (we will fill in the phase angles here)
RecoveredSoln = zeros(d,1);

% Circular indexing function
wrap = @(x) mod(x-1,d)+1;

% Summary of the framework
% We start with the row corresponding to the largest magnitude entry 
% The phases of the next delta successive components can be fixed by 
% looking at the phases of the entries along this row of the matrix
%
% Among these delta components, we pick the one with the largest magnitude.
% This will decide the phase of the next set of components. The process
% repeats until no more phases need to be set.

% TODO: Right now, this only considers phase differences x_ix_j^*, j>i.


% to ensure we do not overwrite already set phase angles
phaseFlag = ones(d, 1);

% Start with the largest magnitude element
mag_copy = full(EstMag);          % Work with a copy
[~, loc] = max(mag_copy);
RecoveredSoln(loc) = 0;
phaseFlag(loc) = 0;
mag_copy(loc) = 0;

while( nnz(phaseFlag) > 0 )

    % This is the delta-block of entries whose phase is to be set
    blockEntries = wrap( [loc-(delta-1):loc-1 loc+1:loc+(delta-1)].' );

    % Now use the phase difference estimates from xx^* to set the phase
    % But we don't want to overwrite already set phases
    RecoveredSoln(blockEntries) = ...
       ( 1-phaseFlag(blockEntries) ).*RecoveredSoln(blockEntries) + ...
       phaseFlag(blockEntries) .* ( RecoveredSoln(loc) - ...
                    angle( BandedMat(loc,blockEntries) ).' );
    phaseFlag(blockEntries) =  0;

    % Update pointers
    [~, maxloc] = max( mag_copy(blockEntries) );
    loc = blockEntries(maxloc);
    mag_copy(loc) = 0;

end 

if (pars.thresholding)
    % Threshold small entries since they can lead to erroneous estimates
    EstMag( abs(EstMag)<=pars.threshold ) = 0;
end

% Final estimate of the recovered vector
RecoveredSoln = EstMag .* exp(1i*RecoveredSoln);        


return