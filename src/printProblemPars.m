% Copyright (c) 2015 Michigan State University and the CHARMS Research
% Group
% 
% This file is part of the BlockPR software package
% 
% Permission is hereby granted, free of charge, to any person obtaining a copy of
% this software and associated documentation files (the "Software"), to deal in
% the Software without restriction, including without limitation the rights to
% use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
% of the Software, and to permit persons to whom the Software is furnished to do
% so, subject to the following conditions:
% 
% The above copyright notice and this permission notice shall be included in all
% copies or substantial portions of the Software.
% 
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
% IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
% OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
% SOFTWARE.

%% printProblemPars.m
%
% This function prints the parameters used in the phase retrieval
% simulation. For details and parameter definitions, see
%
%   Fast Phase Retrieval for High-Dimensions
%   Mark Iwen, Aditya Viswanathan, Yang Wang
%   Jan 2015
%   arXiv:1501.02377
%
% Usage:
%   printProblemPars( pars )
%
% Inputs:
%   pars    -   Matlab structure containing problem parameters.
%               Structure elements are
%                   'd'             - Problem dimension (integer)
%                   'delta'         - Block length/parameter (integer)
%                   'addNoise'      - Noisy simulation? (boolean)
%                   'snr'           - If noisy simulation, added noise SNR 
%                                     (in dB) (double)
%                   'testFncType'   - type of test function (string)
%                   'nComps'        - for particular test functions 
%                                     (integer)
%                   'useFlattening' - use fast JL flattening? (boolean)
%                   'maskType'      - type of masks (string)
%                   'over'          - oversampling factor (double)
%                   'angSyncMethod' - angular synchronization method
%                                     (string)
%                   'threshold'     - noise threshold for angular
%                                     synchronization algorithms (double)
%                   'altProj'       - use alternating projection
%                                     post-processing? (boolean)
%
% Output(s):
%   none
%

% TODO: More descriptive messages for string parameters

function printProblemPars( pars )

% Print out problem parameters
fprintf( '\n\n --------------------------------------------------- \n' );
fprintf(     ' Phase Retrieval from Local Correlation Measurements \n' );
fprintf(     ' --------------------------------------------------- \n' );

fprintf( '\n General Parameters \n' );
fprintf(   ' ------------------ \n' );

fprintf( ' Problem size, d = %d \n', pars.d );

if( pars.addNoise )
    fprintf( ' Noisy simulation? - Yes \n' );
    fprintf( ' Added noise (SNR, dB) = %3.2f \n\n', pars.snr );
else
    fprintf( ' Noisy simulation? - No \n\n' );
end

fprintf( ' Test function type - %s \n', pars.testFncType );
if( pars.useFlattening )
    fprintf( ' Use flattening (fast-JL) transform? - Yes\n' );
end


fprintf( '\n\n Local Measurement Parameters \n' );
fprintf(     ' ---------------------------- \n' );

fprintf( ' Mask/Window length, delta = %d \n', pars.delta );
fprintf( ' Oversampling factor = %3.2f \n', pars.over );

fprintf( ' No. of measurements, D = %3.2fd = %d \n\n', ...
    pars.over*(2*pars.delta-1), round( pars.over*(2*pars.delta-1)*pars.d ) );

fprintf( ' Mask/Window Type - %s \n', pars.maskType );


fprintf( '\n\n Phase Retrieval Algorithm Parameters \n' );
fprintf(     ' ------------------------------------ \n' );

fprintf( ' Phase Retrieval algorithm used: %s \n', pars.alg );

switch pars.alg
    case 'BlockPR'
        fprintf( ' Angular Synchronization method - %s \n', ...
                                                pars.angSyncMethod );
        if( pars.altProj )
            fprintf(' Use alternating projection post-processing? - Yes \n' );
            fprintf(' No. of alternating projection iterations - %d \n', ...
                                        pars.altProjIters );
            fprintf(' Alternating projections tolerance: %3.3e \n', ...
                                        pars.threshold );
        end

    case 'AltProj-GS'
        fprintf( ' No. of iterations - %d \n', pars.altProjIters );
        fprintf( ' Initial guess: %s \n', pars.initGuess );
        fprintf( ' Tolerance/threshold: %3.3e \n', pars.threshold );
end

fprintf( '\n\n' );