% Copyright (c) 2016- Michigan State University and the CHARMS Research
% Group
% 
% This file is part of the BlockPR software package
% 
% Permission is hereby granted, free of charge, to any person obtaining a copy of
% this software and associated documentation files (the "Software"), to deal in
% the Software without restriction, including without limitation the rights to
% use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
% of the Software, and to permit persons to whom the Software is furnished to do
% so, subject to the following conditions:
% 
% The above copyright notice and this permission notice shall be included in all
% copies or substantial portions of the Software.
% 
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
% IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
% OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
% SOFTWARE.

%% Robustness comparison
%
% Script comparing the robustness of various phase retrieval methods.
% 7d (with d=64) global or local correlation measurements are used
%
%   
% This implements Fig. 3a in
%   Fast Phase Retrieval from Local Correlation Measurements
%   Mark Iwen, Aditya Viswanathan, Yang Wang
%   2015
%   arXiv:1501.02377
%

clear all; close all; clc
addpath ../../src/
addpath ../../third/TFOCS/

% For repeatability, set random seed to ...
rng(12345);

% Suppress CVX warnings about soon-to-be deprecated features
warning( 'off', 'MATLAB:nargchk:deprecated' );


%% Execution plot parameters

% SNR values
snrVals = (60:-10:10).';

% Each data point is averaged over...
nTrials = 100;

% Store execution times here
exectime = zeros( length(snrVals), 3 );     % Error and execution time for
                                            % BlockPR, PhaseLift, Alt. Proj

% Store corresponding errors
error    = zeros( length(snrVals), 3 );
                                     

for iSNR = 1:length(snrVals)
    % For each problem size
    
    %% General Simulation Parameters
    d = 64;                         % Problem size
    
    addNoise = true;                % Add noise?
    snr = snrVals(iSNR);
    fprintf( '\n Now runnning simulations at %d dB SNR \n', snr );
    
    testFncType = 'randGauss';      % Type of test function
    useFlattening = false;          % Use flattening (fast JL) transform?

    maskType = 'Fourier';               % Deterministic Fourier-type masks
    delta = 8;                          % No. of measurements is 
                                        % (2delta-1)d = 15d
    over = 1.00;        % No. of measurements = oversampling factor x 
                        %           no. of unknown phase differences
                 
    % Store the above parameters in a structure
    pars = struct(      ...
                        'd',                d, ...
                        ...
                        'addNoise',         addNoise, ...
                        'snr',              snr, ...
                        ...
                        'testFncType',      testFncType, ...
                        'useFlattening',    useFlattening, ...
                        ...
                        'maskType',         maskType, ...
                        'delta',            delta, ...
                        'over',             over, ...
                        'fullMat',          false, ...
                        ...
                        'alg',              'BlockPR', ...
                        'angSyncMethod',    'greedy', ...
                        'altProj',          true, ...
                        'altProjIters',     050, ...
                        'threshold',        1e-10 ...
                        ...
                        );

    % Parameters for Alternating Projections
    pars_GS = struct(      ...
                        'd',                d, ...
                        ...
                        'addNoise',         true, ...
                        'snr',              snr, ...                        
                        ...
                        'testFncType',      'randGauss', ...
                        'useFlattening',    false, ...
                        'fullMat',          false, ...
                        ...
                        'maskType',         'global-randGauss', ...
                        'delta',            4, ...      % 7d global meas.
                        ...
                        'alg',              'AltProj-GS', ...
                        'altProjIters',     1e4, ...
                        'initGuess',        'randn', ...
                        'threshold',        1e-10 ...
                        ...
                        );

    % Parameters for PhaseLift
    % Choose regularization parameter with noise level
    lambdaVals = [1e-2 1e-3 1e-4 1e-5 1e-6 1e-7].';
    pars_PhaseLift = struct(      ...
                        'd',                d, ...
                        ...
                        'addNoise',         true, ...
                        'snr',              snr, ...                        
                        ...
                        'testFncType',      'randGauss', ...
                        'useFlattening',    false, ...
                        ...
                        'maskType',         'global-randGauss', ...
                        'delta',            4, ...      % 7d global meas.
                        'fullMat',          true, ...
                        ...
                        'alg',              'PhaseLift', ...
                        'solver',           'TFOCS', ...          
                        'lambda',           lambdaVals(iSNR) ...
                        ...
                        );

    for itrial = 1:nTrials

        % For each trial...
        
        %% Generate test function, generate masks and measurements
        % Generate desired type of test function
        trueSig = generateTestFunction(pars);

        % Construct masks for generating measurements
        [maskEntries, blockDiag] = constructMask(pars);

        % Generate measurements
        [measurements, pars] = ...
                        generateMeasurements( trueSig, maskEntries, pars );
                

        %% Solve phase retrieval problem
        % ... with BlockPR
        [recoveredSoln_BlockPR, etime_BlockPR] = blockPR( measurements, ...
                                            maskEntries, blockDiag, pars);
        % Correct for global phase factor
        phaseOffset = angle( (recoveredSoln_BlockPR'*trueSig) / ...
                                                (trueSig'*trueSig) );
        recoveredSoln_BlockPR = recoveredSoln_BlockPR*exp(1i*phaseOffset);
        % Reconstruction error
        errordB_BlockPR = 10*log10( norm(trueSig-recoveredSoln_BlockPR)^2 ...
                                                / norm(trueSig)^2 );
        % Store error and execution times
        exectime( iSNR, 1 ) = exectime( iSNR, 1 ) + etime_BlockPR/nTrials;
        error( iSNR, 1 ) = error( iSNR, 1 ) + errordB_BlockPR/nTrials;
        
      
        % ... special measurement construction for global masks
        [~, ~, measurementMat] = constructMask(pars_GS);
        measurements = generateMeasurements( trueSig, ...
                                        measurementMat, pars_GS );
        
        % ... with alternating projections
        [recoveredSoln_AltProj, etime_AltProj] = GerchSax( ...
                        measurements, measurementMat, pars_GS);
        % Correct for global phase factor
        phaseOffset = angle( (recoveredSoln_AltProj'*trueSig) / ...
                                                (trueSig'*trueSig) );
        recoveredSoln_AltProj = recoveredSoln_AltProj * ...
                                                exp(1i*phaseOffset);
        % Reconstruction error
        errordB_AltProj = 10*log10( norm(trueSig-recoveredSoln_AltProj)^2 ...
                                                / norm(trueSig)^2 );
        % Store error and execution times
        exectime( iSNR, 2 ) = exectime( iSNR, 2 ) + etime_AltProj/nTrials;
        error( iSNR, 2 ) = error( iSNR, 2 ) + errordB_AltProj/nTrials;

        
        % ... with phaselift
        [recoveredSoln_PhaseLift, etime_PhaseLift] = PhaseLift( ...
                        measurements, measurementMat, pars_PhaseLift);
        % Correct for global phase factor
        phaseOffset = angle( (recoveredSoln_PhaseLift'*trueSig) / ...
                                                (trueSig'*trueSig) );
        recoveredSoln_PhaseLift = recoveredSoln_PhaseLift * ...
                                                exp(1i*phaseOffset);
        % Reconstruction error
        errordB_PhaseLift = 10*log10( norm(trueSig-recoveredSoln_PhaseLift)^2 ...
                                                / norm(trueSig)^2 );
        % Store error and execution times
        exectime( iSNR, 3 ) = exectime( iSNR, 3 ) + etime_PhaseLift/nTrials;
        error( iSNR, 3 ) = error( iSNR, 3 ) + errordB_PhaseLift/nTrials;
        
    end
    
    % Print out reconstruction SNRs
    fprintf( '   Avg. error for BlockPR is %3.3f dB. \n', error(iSNR, 1) );
    fprintf( '   Avg. error for Alt. Proj. is %3.3f dB. \n', error(iSNR, 2) );
    fprintf( '   Avg. error for PhaseLift is %3.3f dB. \n', error(iSNR, 3) );
end


% Plot figure
plot( snrVals, error(:,1), 'r-+', 'linewidth', 2 ); hold on
plot( snrVals, error(:,2), 'b--o', 'linewidth', 2 );
plot( snrVals, error(:,3), 'g-.*', 'linewidth', 2 );
xlabel( 'Noise Level in SNR (dB)', 'interpreter', 'latex', 'fontsize', 14 )
ylabel( 'Reconstruction Error (in dB)', 'interpreter', ...
                                                'latex', 'fontsize', 14 )
title( 'Robustness to Measurement Noise, $d=64, D=7d$', ...
                                 'interpreter', 'latex', 'fontsize', 14 )
grid; axis([ 5 65 -65 5 ]);
legend( {'BlockPR', 'Alternating Projections', 'PhaseLift'}, ...
        'interpreter', 'latex', 'fontsize', 14, 'location', 'northeast' );
    
% Misc book keeping
warning( 'on', 'MATLAB:nargchk:deprecated' );    