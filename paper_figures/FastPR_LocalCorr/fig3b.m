% Copyright (c) 2016- Michigan State University and the CHARMS Research
% Group
% 
% This file is part of the BlockPR software package
% 
% Permission is hereby granted, free of charge, to any person obtaining a copy of
% this software and associated documentation files (the "Software"), to deal in
% the Software without restriction, including without limitation the rights to
% use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
% of the Software, and to permit persons to whom the Software is furnished to do
% so, subject to the following conditions:
% 
% The above copyright notice and this permission notice shall be included in all
% copies or substantial portions of the Software.
% 
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
% IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
% OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
% SOFTWARE.

%% Robustness as a function of block length delta
%
% Script comparing the robustness of the BlockPR algorithm for various
% values of delta (with d=1024)
%
%   
% This implements Fig. 3b in
%   Fast Phase Retrieval from Local Correlation Measurements
%   Mark Iwen, Aditya Viswanathan, Yang Wang
%   2015
%   arXiv:1501.02377
%

clear all; close all; clc
addpath ../../src/
addpath ../../third/TFOCS/

% For repeatability, set random seed to ...
rng(12345);

% Suppress CVX warnings about soon-to-be deprecated features
warning( 'off', 'MATLAB:nargchk:deprecated' );


%% Plot parameters

% delta factors
deltaFac = [1 2 3].';

% SNR values
snrVals = (60:-10:10).';

% Each data point is averaged over...
nTrials = 100;

% Store errors here
error    = zeros( length(snrVals), length(deltaFac) );
                                     
% For each delta scaling...
for idelta = 1:length(deltaFac)

for iSNR = 1:length(snrVals)
    % For each problem size
    
    %% General Simulation Parameters
    d = 2^10;                       % Problem size
    
    addNoise = true;                % Add noise?
    snr = snrVals(iSNR);
    
    testFncType = 'randGauss';      % Type of test function
    useFlattening = false;          % Use flattening (fast JL) transform?

    delta = floor( deltaFac(idelta)*log2(d) );  % No. of measurements is 
                                                % (2delta-1)d
    overFac = [1 1.5 2];                        % Oversampling factors                                                
    over = overFac(idelta); % No. of measurements = oversampling factor x 
                            %            no. of unknown phase differences

    fprintf( '\n Now runnning simulations for D=%ddlog(d) at %ddB SNR\n', ...
                            round( over*(2*delta-1)/log2(d) ), snr );
                        
    % Store the above parameters in a structure
    masks = {'Fourier', 'random', 'random'};    % Mask types
    pars = struct(      ...
                        'd',                d, ...
                        ...
                        'addNoise',         addNoise, ...
                        'snr',              snr, ...
                        ...
                        'testFncType',      testFncType, ...
                        'useFlattening',    useFlattening, ...
                        ...
                        'maskType',         masks(idelta), ...
                        'delta',            delta, ...
                        'over',             overFac(idelta), ...
                        'fullMat',          false, ...
                        ...
                        'alg',              'BlockPR', ...
                        'angSyncMethod',    'greedy', ...
                        'altProj',          true, ...
                        'altProjIters',     050, ...
                        'threshold',        1e-10 ...
                        ...
                        );

    for itrial = 1:nTrials

        % For each trial...
        
        %% Generate test function, generate masks and measurements
        % Generate desired type of test function
        trueSig = generateTestFunction(pars);

        % Construct masks for generating measurements
        [maskEntries, blockDiag] = constructMask(pars);
        
        % Generate measurements
        [measurements, pars] = ...
                        generateMeasurements( trueSig, maskEntries, pars );
%         [size(measurements,2) 2*log2(d) 6*log2(d) 12*log2(d)]

        %% Solve phase retrieval problem
        recoveredSoln = blockPR( measurements, ...
                                            maskEntries, blockDiag, pars);
        % Correct for global phase factor
        phaseOffset = angle( (recoveredSoln'*trueSig) / ...
                                                (trueSig'*trueSig) );
        recoveredSoln = recoveredSoln*exp(1i*phaseOffset);
        % Reconstruction error
        errordB = 10*log10( norm(trueSig-recoveredSoln)^2 ...
                                                / norm(trueSig)^2 );
        % Store error and execution times
        error( iSNR, idelta ) = error( iSNR, idelta ) + errordB/nTrials;
                        
    end
    
    % Print out reconstruction SNRs
    fprintf( '   Avg. error is %3.3f dB.', error(iSNR, idelta) );
end

fprintf( '\n\n' );

end

% Plot figure
plot( snrVals, error(:,1), 'r-+', 'linewidth', 2 ); hold on
plot( snrVals, error(:,2), 'b--o', 'linewidth', 2 );
plot( snrVals, error(:,3), 'g-.*', 'linewidth', 2 );
xlabel( 'Noise Level in SNR (dB)', 'interpreter', 'latex', 'fontsize', 14 )
ylabel( 'Reconstruction Error (in dB)', 'interpreter', ...
                                                'latex', 'fontsize', 14 )
title( 'Robustness to Measurement Noise, $d=1024$', ...
                                 'interpreter', 'latex', 'fontsize', 14 )
grid; axis([ 5 65 -65 5 ]);
legend( {   'Deterministic Fourier Masks, $D=\lceil2d\log_2d\rceil$', ...
            'Oversampled Random Masks, $D=\lceil2d\log_2d\rceil$', ...
            'Oversampled Random Masks, $D=\lceil2d\log_2d\rceil$'}, ...
        'interpreter', 'latex', 'fontsize', 14, 'location', 'southwest' );
    
% Misc book keeping
warning( 'on', 'MATLAB:nargchk:deprecated' );    