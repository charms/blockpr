This folder contains Matlab scripts to generate figures 
from 

Fast Phase Retrieval from Local Correlation Measurements     
Mark Iwen, Aditya Viswanthan and Yang Wang    
arXiv:1501.02377     
2015
