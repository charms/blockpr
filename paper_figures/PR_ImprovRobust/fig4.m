% Copyright (c) 2016- Michigan State University, University of California 
% - San Diego, University of Michigan - Dearborn and the CHARMS Research Group
% 
% This file is part of the BlockPR software package
% 
% Permission is hereby granted, free of charge, to any person obtaining a copy of
% this software and associated documentation files (the "Software"), to deal in
% the Software without restriction, including without limitation the rights to
% use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
% of the Software, and to permit persons to whom the Software is furnished to do
% so, subject to the following conditions:
% 
% The above copyright notice and this permission notice shall be included in all
% copies or substantial portions of the Software.
% 
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
% IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
% OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
% SOFTWARE.

%% Error vs Iteration count - HIO+ER
%
% Script plotting out the error vs iteration count for HIO+ER
%
% This implements Fig. 4 in
%   PHASE RETRIEVAL FROM LOCAL MEASUREMENTS: IMPROVED ROBUSTNESS VIA 
%   EIGENVECTOR-BASED ANGULAR SYNCHRONIZATION
%   MARK A. IWEN, BRIAN PRESKITT, RAYAN SAAB, ADITYA VISWANATHAN
%   2016
%   arXiv:1612.01182
%

clear; close all; clc
addpath ../../src/

% For repeatability, set random seed to ...
rng(12345);


%% Simulation parameters

% Each data point is averaged over...
nTrials = 100;

% Max. no. of iterations
maxIT = 2e3;

% HIO vs ER iterations
nHIO = 20;
nER = 10;

% HIO+ER Block Size
blk_size = nHIO+nER;

% Store errors and exec times
error    = zeros( floor(maxIT/blk_size), 2 );
etime    = zeros( floor(maxIT/blk_size), 2 );


% General Simulation Parameters
d = 64;                         % Problem size

addNoise = true;                % Add noise?
snr = 40;                       % ... @ 40dB
testFncType = 'randGauss';      % Type of test function
useFlattening = false;          % Use flattening (fast JL) transform?

maskType = 'Fourier';             % Fourier (ptych.) Measurement masks
delta = 4;                        % No. of measurements is 
                                  % (2delta-1)d = 7d
over = 1.00;        % No. of measurements = oversampling factor x 
                    %           no. of unknown phase differences


% Note: For completeness, ER errors are also evaluated (although not plotted)                    
                    

for iter = blk_size:blk_size:maxIT
                    
    fprintf( '\n Now recording average error after %d iterations ... \n', iter );
    
    % Parameters for Gerchberg-Saxton/Error Reduction
    pars_ER = struct(      ...
                        'd',                d, ...
                        ...
                        'addNoise',         true, ...
                        'snr',              snr, ...                        
                        ...
                        'testFncType',      'randGauss', ...
                        'useFlattening',    false, ...
                        ...
                        'maskType',         maskType, ...
                        'delta',            delta, ...
                        'over',             over, ...
                        'fullMat',          false, ...                        
                        ...
                        'alg',              'AltProj-GS', ...
                        'altProjIters',     iter, ...
                        'initGuess',        'zeros', ...
                        'threshold',        1e-10 ...
                        ...
                        );
                    
    % Parameters for Hybrid Input Output (HIO)
    pars_HIO = struct(      ...
                        'd',                d, ...
                        ...
                        'addNoise',         true, ...
                        'snr',              snr, ...                        
                        ...
                        'testFncType',      'randGauss', ...
                        'useFlattening',    false, ...
                        ...
                        'maskType',         maskType, ...
                        'delta',            delta, ...
                        'over',             over, ...
                        'fullMat',          false, ...                        
                        ...
                        'alg',              'AltProj-GS', ...
                        'altProjIters',     iter, ...
                        'niter_HIO',        nHIO, ...
                        'niter_ER',         nER, ...                        
                        'initGuess',        'zeros', ...
                        'threshold',        1e-10 ...
                        ...
                        );
                    

    for itrial = 1:nTrials

        % For each trial...
        
        %% Generate test function, generate masks and measurements
        % Generate desired type of test function
        trueSig = generateTestFunction(pars_HIO);

        % Construct masks for generating measurements
        [maskEntries, blockDiag] = constructMask(pars_HIO);

        % Generate measurements
        [measurements, pars_HIO] = ...
                generateMeasurements( trueSig, maskEntries, pars_HIO );
        % Record noise power for other algorithms
        pars_ER.noise_power = pars_HIO.noise_power;
                

        %% Solve phase retrieval problem                
        
        % ... with alternating projections (ER)
        [recoveredSoln_AltProj, etime_ER] = GerchSax( ...
                        measurements, maskEntries, pars_ER);
        % Correct for global phase factor
        phaseOffset = angle( (recoveredSoln_AltProj'*trueSig) / ...
                                                (trueSig'*trueSig) );
        recoveredSoln_AltProj = recoveredSoln_AltProj * ...
                                                exp(1i*phaseOffset);
        % Reconstruction error
        errordB_AltProj = 10*log10( norm(trueSig-recoveredSoln_AltProj)^2 ...
                                                / norm(trueSig)^2 );
        % Store error and exec time
        error( iter/blk_size, 1 ) = error( iter/blk_size, 1 ) + errordB_AltProj/nTrials;
        etime( iter/blk_size, 1 ) = etime( iter/blk_size, 1 ) + etime_ER/nTrials;

        
        % ... with alternating projections (HIO)
        [recoveredSoln_AltProj, etime_HIO] = HIO( ...
                        measurements, maskEntries, pars_HIO);
        % Correct for global phase factor
        phaseOffset = angle( (recoveredSoln_AltProj'*trueSig) / ...
                                                (trueSig'*trueSig) );
        recoveredSoln_AltProj = recoveredSoln_AltProj * ...
                                                exp(1i*phaseOffset);
        % Reconstruction error
        errordB_AltProj = 10*log10( norm(trueSig-recoveredSoln_AltProj)^2 ...
                                                / norm(trueSig)^2 );
        % Store error
        error( iter/blk_size, 2 ) = error( iter/blk_size, 2 ) + errordB_AltProj/nTrials;
        etime( iter/blk_size, 2 ) = etime( iter/blk_size, 2 ) + etime_HIO/nTrials;
                        
        
    end
    
    % Print out reconstruction SNRs
%     fprintf( '   Avg. error for Alt. Proj. (ER) is %3.3f dB. \n', error(iter/blk_size, 1) );
%     fprintf( '   Avg. exec time for Alt. Proj. (ER) is %3.3f s. \n', etime(iter/blk_size, 1) );    
    fprintf( '   Avg. error for Alt. Proj. (HIO+ER) is %3.3f dB. \n', error(iter/blk_size, 2) );
    fprintf( '   Avg. exec time for Alt. Proj. (HIO+ER) is %3.3f s. \n', etime(iter/blk_size, 2) );        

    % Save results
%     save( 'fig4.mat', 'blk_size', 'maxIT', 'error' );
    
end


% Plot figure
% plot( blk_size:blk_size:maxIT, error(:,1), '--+', 'linewidth', 2 ); hold on
plot( blk_size:blk_size:maxIT, error(:,2), '--o', 'linewidth', 2 );
xlabel( 'No. of iterations', 'interpreter', 'latex', 'fontsize', 14 )
ylabel( 'Reconstruction Error (in dB)', 'interpreter', ...
                                                'latex', 'fontsize', 14 )
title( 'Error vs Iteration Count at 40dB SNR, $d=64, D=7d$', ...
                                 'interpreter', 'latex', 'fontsize', 14 )
grid; axis([ 0 2000 -35 0 ]);
legend( {'$(n_{HIO}^{iter}, n_{ER}^{iter}) = (20,10)$'}, ...
        'interpreter', 'latex', 'fontsize', 14, 'location', 'northeast' );
    
