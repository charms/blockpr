% Copyright (c) 2016- Michigan State University, University of California 
% - San Diego, University of Michigan - Dearborn and the CHARMS Research Group
% 
% This file is part of the BlockPR software package
% 
% Permission is hereby granted, free of charge, to any person obtaining a copy of
% this software and associated documentation files (the "Software"), to deal in
% the Software without restriction, including without limitation the rights to
% use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
% of the Software, and to permit persons to whom the Software is furnished to do
% so, subject to the following conditions:
% 
% The above copyright notice and this permission notice shall be included in all
% copies or substantial portions of the Software.
% 
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
% IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
% OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
% SOFTWARE.

%% Robustness comparison
%
% Script comparing the robustness of various phase retrieval methods.
% 15d (with d=64) local correlation measurements are used
%
%   
% This implements Fig. 3b in
%   PHASE RETRIEVAL FROM LOCAL MEASUREMENTS: IMPROVED ROBUSTNESS VIA 
%   EIGENVECTOR-BASED ANGULAR SYNCHRONIZATION
%   MARK A. IWEN, BRIAN PRESKITT, RAYAN SAAB, ADITYA VISWANATHAN
%   2016
%   arXiv:1612.01182
%

clear; close all; clc
addpath ../../src/

% Add path to local installation of CVX
addpath ~/software/cvx

% For repeatability, set random seed to ...
rng(1234);

% Suppress CVX warnings about soon-to-be deprecated features
warning( 'off', 'MATLAB:nargchk:deprecated' );
% Suppress matrix conditioning warnings (not applicable for the inverse
% power method used here)
warning( 'off', 'MATLAB:nearlySingularMatrix' );


%% Execution plot parameters

% SNR values
snrVals = (60:-10:10).';

% Each data point is averaged over...
nTrials = 100;

% HIO+ER iterations
niter = 600;
npp_iter = 60;
nHIO = 20;
nER = 10;

% Store execution times here
exectime = zeros( length(snrVals), 5 );     % Error and execution time for
                                            % BlockPR, PhaseLift, Alt. Proj

% Store corresponding errors
error    = zeros( length(snrVals), 5 );
                                     

for iSNR = 1:length(snrVals)
    % For each problem size
    
    %% General Simulation Parameters
    d = 64;                         % Problem size
    
    addNoise = true;                % Add noise?
    snr = snrVals(iSNR);
    fprintf( '\n Now runnning simulations at %d dB SNR \n', snr );
    
    testFncType = 'randGauss';      % Type of test function
    useFlattening = false;          % Use flattening (fast JL) transform?

    maskType = 'rayan';               % Sparse Measurement masks
    delta = 8;                        % No. of measurements is 
                                      % (2delta-1)d = 15d
    over = 1.00;        % No. of measurements = oversampling factor x 
                        %           no. of unknown phase differences
                 
    % Store the above parameters in a structure
    pars_Alg1 = struct(      ...
                        'd',                d, ...
                        ...
                        'addNoise',         addNoise, ...
                        'snr',              snr, ...
                        ...
                        'testFncType',      testFncType, ...
                        'useFlattening',    useFlattening, ...
                        ...
                        'maskType',         maskType, ...
                        'delta',            delta, ...
                        'over',             over, ...
                        'fullMat',          false, ...                        
                        ...
                        'alg',              'BlockPR', ...
                        'angSyncMethod',    'eig', ...
                        'MagEstType',       'diag', ...                        
                        'altProj',          false, ...
                        'altProjIters',     100, ...
                        'threshold',        1e-10 ...
                        ...
                        );
                    
    % BlockPR - proposed Alg. 1 w/ Mag. Est + Alt. Proj (ER)
    pars_PP = struct(      ...
                        'd',                d, ...
                        ...
                        'addNoise',         addNoise, ...
                        'snr',              snr, ...
                        ...
                        'testFncType',      testFncType, ...
                        'useFlattening',    useFlattening, ...
                        ...
                        'maskType',         maskType, ...
                        'delta',            delta, ...
                        'over',             over, ...
                        'fullMat',          false, ...                        
                        ...
                        'alg',              'BlockPR', ...
                        'angSyncMethod',    'eig', ...
                        'MagEstType',       'diag', ...
                        'altProj',          true, ...                        
                        'altProjIters',     npp_iter, ...
                        'niter_HIO',        nHIO, ...
                        'niter_ER',         nER, ...                         
                        'threshold',        1e-10 ...
                        ...
                        );
                    
                    
    % Parameters for Gerchberg-Saxton/Error Reduction
    pars_ER = struct(      ...
                        'd',                d, ...
                        ...
                        'addNoise',         true, ...
                        'snr',              snr, ...                        
                        ...
                        'testFncType',      'randGauss', ...
                        'useFlattening',    false, ...
                        ...
                        'maskType',         'rayan', ...
                        'delta',            delta, ...
                        'over',             over, ...
                        'fullMat',          false, ...                        
                        ...
                        'alg',              'AltProj-GS', ...
                        'altProjIters',     6.0e3, ...
                        'initGuess',        'zeros', ...
                        'threshold',        1e-10 ...
                        ...
                        );
                    
    % Parameters for Hybrid Input Output (HIO)
    pars_HIO = struct(      ...
                        'd',                d, ...
                        ...
                        'addNoise',         true, ...
                        'snr',              snr, ...                        
                        ...
                        'testFncType',      'randGauss', ...
                        'useFlattening',    false, ...
                        ...
                        'maskType',         maskType, ...
                        'delta',            delta, ...
                        'over',             over, ...
                        'fullMat',          false, ...                        
                        ...
                        'alg',              'AltProj-GS', ...
                        'altProjIters',     niter, ...
                        'niter_HIO',        nHIO, ...
                        'niter_ER',         nER, ...                        
                        'initGuess',        'zeros', ...
                        'threshold',        1e-10 ...
                        ...
                        );
                    

    % Parameters for PhaseLift
    % Choose regularization parameter with noise level
    lambdaVals = [1e-2 1e-3 1e-4 1e-5 1e-6 1e-7].';
    pars_PhaseLift = struct(      ...
                        'd',                d, ...
                        ...
                        'addNoise',         true, ...
                        'snr',              snr, ...                        
                        ...
                        'testFncType',      'randGauss', ...
                        'useFlattening',    false, ...
                        ...
                        'maskType',         'rayan', ...
                        'delta',            delta, ...
                        'over',             over, ...
                        'fullMat',          true, ...                        
                        ...
                        'alg',              'PhaseLift', ...
                        'solver',           'CVX', ...          
                        'lambda',           lambdaVals(iSNR) ...
                        ...
                        );

    for itrial = 1:nTrials

        % For each trial...
        
        %% Generate test function, generate masks and measurements
        % Generate desired type of test function
        trueSig = generateTestFunction(pars_Alg1);

        % Construct masks for generating measurements
        [maskEntries, blockDiag] = constructMask(pars_Alg1);

        % Generate measurements
        [measurements, pars_Alg1] = ...
                generateMeasurements( trueSig, maskEntries, pars_Alg1 );
        % Record noise power for other algorithms
        pars_ER.noise_power = pars_Alg1.noise_power;
        pars_HIO.noise_power = pars_Alg1.noise_power;
        pars_PP.noise_power = pars_Alg1.noise_power;
        pars_PhaseLift.noise_power = pars_Alg1.noise_power;
                

        %% Solve phase retrieval problem        
        % ... with BlockPR (Alg. 1)
        [recoveredSoln_BlockPR, etime_BlockPR] = blockPR( measurements, ...
                                        maskEntries, blockDiag, pars_Alg1);
        % Correct for global phase factor
        phaseOffset = angle( (recoveredSoln_BlockPR'*trueSig) / ...
                                                (trueSig'*trueSig) );
        recoveredSoln_BlockPR = recoveredSoln_BlockPR*exp(1i*phaseOffset);
        % Reconstruction error
        errordB_BlockPR = 10*log10( norm(trueSig-recoveredSoln_BlockPR)^2 ...
                                                / norm(trueSig)^2 );
        % Store error and execution times
        exectime( iSNR, 3 ) = exectime( iSNR, 3 ) + etime_BlockPR/nTrials;
        error( iSNR, 3 ) = error( iSNR, 3 ) + errordB_BlockPR/nTrials;
        
        
        
        % ... with BlockPR (Alg. 1 + post-processing)
        [recoveredSoln_BlockPR, etime_BlockPR] = blockPR( measurements, ...
                                        maskEntries, blockDiag, pars_PP);
        % Correct for global phase factor
        phaseOffset = angle( (recoveredSoln_BlockPR'*trueSig) / ...
                                                (trueSig'*trueSig) );
        recoveredSoln_BlockPR = recoveredSoln_BlockPR*exp(1i*phaseOffset);
        % Reconstruction error
        errordB_BlockPR = 10*log10( norm(trueSig-recoveredSoln_BlockPR)^2 ...
                                                / norm(trueSig)^2 );
        % Store error and execution times
        exectime( iSNR, 4 ) = exectime( iSNR, 4 ) + etime_BlockPR/nTrials;
        error( iSNR, 4 ) = error( iSNR, 4 ) + errordB_BlockPR/nTrials;

        
        
        % ... with alternating projections (ER)
        [recoveredSoln_AltProj, etime_AltProj] = GerchSax( ...
                        measurements, maskEntries, pars_ER);
        % Correct for global phase factor
        phaseOffset = angle( (recoveredSoln_AltProj'*trueSig) / ...
                                                (trueSig'*trueSig) );
        recoveredSoln_AltProj = recoveredSoln_AltProj * ...
                                                exp(1i*phaseOffset);
        % Reconstruction error
        errordB_AltProj = 10*log10( norm(trueSig-recoveredSoln_AltProj)^2 ...
                                                / norm(trueSig)^2 );
        % Store error and execution times
        exectime( iSNR, 1 ) = exectime( iSNR, 1 ) + etime_AltProj/nTrials;
        error( iSNR, 1 ) = error( iSNR, 1 ) + errordB_AltProj/nTrials;


        
        % ... with alternating projections (HIO)
        [recoveredSoln_AltProj, etime_AltProj] = HIO( ...
                        measurements, maskEntries, pars_HIO);
        % Correct for global phase factor
        phaseOffset = angle( (recoveredSoln_AltProj'*trueSig) / ...
                                                (trueSig'*trueSig) );
        recoveredSoln_AltProj = recoveredSoln_AltProj * ...
                                                exp(1i*phaseOffset);
        % Reconstruction error
        errordB_AltProj = 10*log10( norm(trueSig-recoveredSoln_AltProj)^2 ...
                                                / norm(trueSig)^2 );
        % Store error and execution times
        exectime( iSNR, 2 ) = exectime( iSNR, 2 ) + etime_AltProj/nTrials;
        error( iSNR, 2 ) = error( iSNR, 2 ) + errordB_AltProj/nTrials;

        
        
        % ... with phaselift
        % ... special measurement construction fo PhaseLift
        [~, ~, measurementMat] = constructMask(pars_PhaseLift);
        measurements = generateMeasurements( trueSig, ...
                                    measurementMat, pars_PhaseLift );

        [recoveredSoln_PhaseLift, etime_PhaseLift] = PhaseLift( ...
                        measurements, measurementMat, pars_PhaseLift);
        % Correct for global phase factor
        phaseOffset = angle( (recoveredSoln_PhaseLift'*trueSig) / ...
                                                (trueSig'*trueSig) );
        recoveredSoln_PhaseLift = recoveredSoln_PhaseLift * ...
                                                exp(1i*phaseOffset);
        % Reconstruction error
        errordB_PhaseLift = 10*log10( norm(trueSig-recoveredSoln_PhaseLift)^2 ...
                                                / norm(trueSig)^2 );
        % Store error and execution times
        exectime( iSNR, 5 ) = exectime( iSNR, 5 ) + etime_PhaseLift/nTrials;
        error( iSNR, 5 ) = error( iSNR, 5 ) + errordB_PhaseLift/nTrials;
        
    end
    
    % Print out reconstruction SNRs
    fprintf( '   Avg. error for Alt. Proj. (ER) is %3.3f dB. \n', error(iSNR, 1) );
    fprintf( '   Avg. error for Alt. Proj. (HIO) is %3.3f dB. \n', error(iSNR, 2) );
    fprintf( '   Avg. error for BlockPR (Alg. 1) is %3.3f dB. \n', error(iSNR, 3) );
    fprintf( '   Avg. error for BlockPR (Alg. 1, post-processed) is %3.3f dB. \n', error(iSNR, 4) );
    fprintf( '   Avg. error for PhaseLift is %3.3f dB. \n', error(iSNR, 5) );

    % Save results
%     save( 'fig3b.mat', 'snrVals', 'error' );
end


% Plot figure
plot( snrVals, error(:,1), '--+', 'linewidth', 2 ); hold on
plot( snrVals, error(:,2), '--o', 'linewidth', 2 );
plot( snrVals, error(:,3), '-s', 'linewidth', 2 );
plot( snrVals, error(:,4), '-x', 'linewidth', 2 );
plot( snrVals, error(:,5), '-.^', 'linewidth', 2 );
xlabel( 'Noise Level in SNR (dB)', 'interpreter', 'latex', 'fontsize', 14 )
ylabel( 'Reconstruction Error (in dB)', 'interpreter', ...
                                                'latex', 'fontsize', 14 )
title( 'Robustness to Measurement Noise, $d=64, D=15d$', ...
                                 'interpreter', 'latex', 'fontsize', 14 )
grid; axis([ 5 65 -65 5 ]);
legend( {'Gerchberg--Saxton (ER, 6000 iters.)', 'HIO+ER (600 iters.)', 'BlockPR (Alg. 1)', ...
            'BlockPR (w/ 60 iters. HIO+ER)', 'PhaseLift'}, ...
        'interpreter', 'latex', 'fontsize', 14, 'location', 'southwest' );
    
% Misc book keeping
% Turn back on matrix conditioning warnings
warning( 'on', 'MATLAB:nearlySingularMatrix' );
% ... and others
warning( 'on', 'MATLAB:nargchk:deprecated' );

