# BlockPR: Matlab Software for Phase Retrieval from Local Correlation Measurements

This repository contains Matlab code for solving the 
phase retrieval problem. Details of the method, 
theoretical guarantees and representative numerical 
results can be found in the following manuscripts

Fast Phase Retrieval from Local Correlation Measurements [arXiv][arXiv_FastPR]         
Mark Iwen, Aditya Viswanthan and Yang Wang    
arXiv:1501.02377     
2015

Phase Retrieval from Local Measurements: Improved Robustness via 
Eigenvector-Based Angular Synchronization [arXiv][arXiv_PREig]         
Mark A. Iwen, Brian Preskitt, Rayan Saab, Aditya Viswanathan       
arXiv:1612.01182       
2016

This software was developed at the [Department of 
Mathematics][msumath], [Michigan State University][msu] 
and the [Department of Mathematics][ucsdmath], [University of 
California - San Diego][ucsd] and is released under the MIT license.

The software was developed and tested using Matlab 
R2016b. 


## Directory Structure and Contents

The software package is organized under the following 
directory structure:

 - demos/    
   This folder contains a representative implementation 
   of the algorithm discussed in the paper. If you have 
   just downloaded the software, execute 
   phaseRetrieval_blockPR.m from this folder in Matlab.

 - src/     
   This folder contains auxiliary functions necessary to 
   implement the algorithm. 

 - third/      
   This folder contains third party software used to implement
   other popular phase retrieval algorithms and perform subsequent
   performance comparisons. In particular, the folder contains
   an installation of TFOCS with minor modifications (to the
   tfocs_initialize.m routine); this is primarily used
   to implement algorithms such as PhaseLift. Please see the
   respective folder for licensing details.

 - paper_figures/      
   This folder contains Matlab scripts for generating 
   figures from the associated paper(s).


## Instructions

Extract the contents of the zip file and execute, in 
Matlab, scripts from the demos/ or paper_figures/ folders. 


## Contact

Bug reports, comments and suggestions are welcome 
at the [BlockPR Bitbucket repository][bitbucket]. 


[msu]: http://www.msu.edu/
[ucsd]: https://www.ucsd.edu/
[msumath]: http://math.msu.edu/
[ucsdmath]: https://math.ucsd.edu/
[tfocs]: http://cvxr.com/tfocs/
[mark]: http://www.math.msu.edu/~markiwen/
[yang]: http://www.math.ust.hk/~yangwang/
[bitbucket]: https://bitbucket.org/charms/blockpr/
[arXiv_FastPR]: http://arxiv.org/abs/1501.02377
[arXiv_PREig]: http://arxiv.org/abs/1612.01182
