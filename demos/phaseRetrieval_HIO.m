% Copyright (c) 2014- Michigan State University and the CHARMS Research
% Group
% 
% This file is part of the BlockPR software package
% 
% Permission is hereby granted, free of charge, to any person obtaining a copy of
% this software and associated documentation files (the "Software"), to deal in
% the Software without restriction, including without limitation the rights to
% use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
% of the Software, and to permit persons to whom the Software is furnished to do
% so, subject to the following conditions:
% 
% The above copyright notice and this permission notice shall be included in all
% copies or substantial portions of the Software.
% 
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
% IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
% OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
% SOFTWARE.

%% Phase Retrieval using the Gerchberg-Saxton Alternating Projections
%
% Script to implement phase retrieval from local correlation measurements 
% using the Gerchberg-Saxton alternating projection algorithm.
%
% We solve the phase recovery problem
%
%   find x      s.t.        |M x|^2 = b, 
%
% where
%   x \in C^d is the unknown signal
%   b \in R^d are the magnitude measurements
%   M \in C^{Dxd} is block-circulant, with 
%                [     M_1     ]
%                [     M_2     ]
%         M =    [      .      ]
%                [      .      ]
%                [      .      ]
%                [ M_{2delta-1}]
%    where 
%       M_i \in C^{dxd} are circulant matrices describing correlations 
%       with local (compactly-supported) masks. 
%
%

clear; close all; clc
addpath ../src/

% For repeatability, set random seed to ...
rng(12345);


%% Simulation Parameters
d = 200;                  % Dimension of the problem

% ------------------------------------------------------
% Noise Parameters
% ------------------------------------------------------
% Noisy data?
% addNoise = true;
addNoise = false;

% Desired noise level (SNR, in dB) for noisy simulations
snr = 30;

% ------------------------------------------------------
% Test Function Definition
% ------------------------------------------------------
% Type of test function
testFncType = 'randGauss';
% testFncType = 'uniRand';
% testFncType = 'sparse';
% testFncType = 'sinusoid';

% No. of components in test function
% This applies for sparse test functions or sinusoids
% For sparse test functions, this is the number of non-zero entries
% For sinusoids, this is the number of frequencies
nComps = 10;

% Use flattening (fast JL) transform?
% The block-circulant construction used here does not work with sparse 
% signals. For such signals, flatten the test signal and apply the
% algorithm to the result
% useFlattening = true;
useFlattening = false;


% ------------------------------------------------------
% Measurement Mask/Window Definition and Parameters
% ------------------------------------------------------
% Mask Type (Random vs Deterministic/Fourier-type)
% maskType = 'random';
maskType = 'Fourier';

% Band/locality parameter, delta
% (No. of non-zero entries in mask is delta)
delta = round( 1.35*log2(d) );

% Oversampling factor
over = 1.00;        % No. of measurements = oversampling factor x 
                    %           no. of unknown phase differences

                    

% ------------------------------------------------------
% Reconstruction Algorithm
% ------------------------------------------------------

% Which phase retrieval algorithm do you want to use?
alg = 'AltProj-HIO';


                    
% ------------------------------------------------------
% Gerchberg-Saxton parameters
% ------------------------------------------------------

% No. of iterations of alt. projection to apply
altProjIters = 1e4;

% Initialization
% initGuess = 'zeros';          % Zeros
initGuess = 'randn';            % Complex standard Gaussian
% initGuess = 'std_basis';      % (1st) Standard basis vector

% Threshold
threshold = 1e-10;              % If successive iterates are smaller than 
                                % the threshold, algorithm terminates 

% Store the above parameters in a structure for ease of use with functions
pars = struct(      ...
                    'd',                d, ...
                    ...
                    'addNoise',         addNoise, ...
                    'snr',              snr, ...
                    ...
                    'testFncType',      testFncType, ...
                    'nComps',           nComps, ...
                    'useFlattening',    useFlattening, ...
                    ...
                    'maskType',         maskType, ...
                    'delta',            delta, ...
                    'over',             over, ...
                    'fullMat',          false, ...
                    ...
                    'alg',              alg, ...
                    'altProjIters',     altProjIters, ...
                    'initGuess',        initGuess, ...
                    'threshold',        threshold ...
                    ...
                    );

% Print out problem parameters
printProblemPars(pars);


%% Generate test function, generate masks and measurements

% Generate desired type of test function
trueSig = generateTestFunction(pars);

% Construct masks for generating measurements
[maskEntries, blockDiag] = constructMask(pars);

% Generate measurements
[measurements, pars] = generateMeasurements( trueSig, maskEntries, pars );

                
%% Phase Retrieval using the (Gerchberg-Saxton) alternating projections
%

% Initialization
switch initGuess
    
    case 'zeros'
        recoveredSoln = zeros(d, 1);
    case 'randn'
        recoveredSoln = sqrt(1/2)*( randn(d,1) + 1i*randn(d, 1) );
    case 'std_basis'
        recoveredSoln = zeros(d, 1);
        recoveredSoln(1) = 1;
        
end

tic
recoveredSoln = HIO_BlockPR( ...
                recoveredSoln, ...      % Initial guess
                measurements, ...       % Measurements
                maskEntries, ...        % Measurement masks
                pars ...                % Problem parameters
                );
exectime = toc;

fprintf( ' Execution time is %3.3e secs.\n', exectime );

%% Plot Reconstruction

% Physical space nodes
nodes = (0:d-1).';

% Correct for global pahse factor
phaseOffset = angle( (recoveredSoln'*trueSig) / ...
                                        (trueSig'*trueSig) );
recoveredSoln = recoveredSoln * exp(1i*phaseOffset) ;

% Plot the real part of signal
figure; subplot(1,2,1)
stem( nodes, real(trueSig), 'ro', 'linewidth', 2 ); hold on
stem( nodes, real(recoveredSoln), 'b--+', 'linewidth', 2 );
xlabel n; ylabel x(n); 
title 'Phase Retrieval using Correlations - Real Part'
legend('True', 'Recovered'); grid

% ... and the imaginary part
subplot(1,2,2)
stem( nodes, imag(trueSig), 'ro', 'linewidth', 2 ); hold on
stem( nodes, imag(recoveredSoln), 'b--+', 'linewidth', 2 );
xlabel n; ylabel x(n); 
title 'Phase Retrieval using Correlations - Imaginary Part'
legend('True', 'Recovered'); grid

% Reconstruction error
errordB = 10*log10( norm(trueSig-recoveredSoln)^2 ...
                                / norm(trueSig)^2 );
fprintf( '\n 2 Norm Error in reconstruction is %3.3e or %3.2f dB', ...
        norm(trueSig-recoveredSoln)/norm(trueSig), errordB );
fprintf( '\n Inf. Norm Error in reconstruction is %3.3e\n\n', ...
        norm(trueSig-recoveredSoln,inf)/norm(trueSig,inf) );